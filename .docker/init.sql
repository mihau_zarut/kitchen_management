create database rms_database;
create user rms_user with encrypted password 'rms_database_pass';
grant all privileges on database rms_database to rms_user;
alter user rms_user createdb;
