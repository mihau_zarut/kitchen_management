FROM python:3

ARG USER_ID
ARG GROUP_ID

RUN getent group $GROUP_ID || groupadd rms_user -g $GROUP_ID
RUN useradd --create-home rms_user -u $USER_ID -g $GROUP_ID

ENV BASE_PATH "/home/rms_user"
WORKDIR "${BASE_PATH}/code"

USER rms_user

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH "${PYTHONPATH}:${BASE_PATH}/code"
ENV PATH "${PATH}:${BASE_PATH}/code:${BASE_PATH}/.local/bin"

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY . "${BASE_PATH}/code"
