import debug_toolbar
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

urlpatterns = [
    path('admin/', admin.site.urls),
    path('mvt/', include('recipes.urls')),
    path('api/', include('recipes.api.urls')),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.authtoken')),
]

if settings.DEBUG:
    schema_view = get_schema_view(
        openapi.Info(
            title="Kitchen Management",
            default_version='v1',
            description="Test description",
        ),
        public=True,
        permission_classes=[permissions.AllowAny, ],
    )

    urlpatterns += [
        path('__debug__', include(debug_toolbar.urls)),
        re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
        re_path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]
