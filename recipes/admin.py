from django.contrib import admin
from .models import Product, Ingredient, Recipe


class ProductAdmin(admin.ModelAdmin):
    fields = [
        'name',
        'available_quantity',
        'unit',
        'price_per_unit'
    ]
    search_fields = [
        'name',
    ]
    list_display = [
        'name',
        'available_quantity',
        'unit',
        'price_per_unit'
    ]

    list_filter = [
        'name'
    ]


class IngredientInline(admin.TabularInline):
    model = Ingredient
    extra = 0
    verbose_name = 'Ingredients'
    fields = [
        'product',
        'quantity',
        'units'
    ]


class RecipeAdmin(admin.ModelAdmin):
    # Example actions: make unavailable to make if one of ingredients is 0

    fields = [
        'name',
        'description',
        'prep_time_in_minutes',
        'cook_time_in_minutes',
        'cost_of_ingredients',
        # 'ingredients',
        'available_to_prepare'
    ]
    search_fields = [
        'name',
        'ingredients',
    ]
    list_display = [
        'name',
        'get_short_description',
        'prep_time_in_minutes',
        'cook_time_in_minutes',
        'ingredients_number',
        'available_to_prepare',
        'cost_of_ingredients'

    ]
    readonly_fields = ['cost_of_ingredients', ]
    list_filter = [
        'name'
    ]

    def get_short_description(self, obj):
        description = obj.description or ''
        return description[:50] + '...'

    get_short_description.short_description = 'Short description'

    inlines = [IngredientInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if instance.quantity > instance.product.available_quantity:
                instance.recipe.available_to_prepare = False
                instance.save()
                instance.recipe.save()
        super().save_formset(request, form, formset, change)


admin.site.register(Product, ProductAdmin)
admin.site.register(Ingredient)
admin.site.register(Recipe, RecipeAdmin)
