from rest_framework import serializers
from rest_framework.serializers import PrimaryKeyRelatedField

from recipes.models import Recipe, Ingredient, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'name',
            'available_quantity',
            'unit',
            'price_per_unit'
        ]


class IngredientSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    # recipe = RecipeSerializer()

    class Meta:
        model = Ingredient

        fields = [
            'product',
            # 'recipe',
            'quantity',
            'units'
        ]


class RecipeSerializer(serializers.ModelSerializer):
    # ingredients = IngredientSerializer(many=True)
    ingredients = PrimaryKeyRelatedField(many=True, queryset=Ingredient.objects.all())

    class Meta:
        model = Recipe
        fields = [
            'name',
            'description',
            'prep_time_in_minutes',
            'cook_time_in_minutes',
            'available_to_prepare',
            'ingredients',
            'cost_of_ingredients'
        ]
        depth = 2
