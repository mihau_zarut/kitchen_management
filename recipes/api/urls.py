from recipes.api.views import ProductViewSet, IngredientViewSet, RecipeViewSet
from rest_framework.routers import DefaultRouter

app_name = 'api'

router = DefaultRouter()
router.register(r'product', ProductViewSet)
router.register(r'recipe', RecipeViewSet)
urlpatterns = router.urls
