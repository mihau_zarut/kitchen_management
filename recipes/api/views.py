from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet
from recipes.api.serializers import ProductSerializer, IngredientSerializer, RecipeSerializer
from recipes.models import Product, Ingredient, Recipe


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 20


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    pagination_class = StandardResultsSetPagination
    serializer_class = ProductSerializer


class IngredientViewSet(ModelViewSet):
    queryset = Ingredient.objects.all()
    pagination_class = StandardResultsSetPagination
    serializer_class = IngredientSerializer


class RecipeViewSet(ModelViewSet):
    queryset = Recipe.objects.all()
    pagination_class = StandardResultsSetPagination
    serializer_class = RecipeSerializer

# TODO create/update or nested writable serializer
