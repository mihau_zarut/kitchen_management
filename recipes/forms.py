from django import forms
from django.forms import inlineformset_factory, widgets

from .models import Ingredient, Recipe, Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            'name',
            'available_quantity',
            'unit',
            'price_per_unit'
        ]


class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        exclude = ['available_to_prepare', 'cost_of_ingredients']


IngredientFormSet = inlineformset_factory(Recipe,
                                          Ingredient,
                                          fields=['product', 'quantity', ],  # 'units', ],
                                          can_delete=True,
                                          extra=1,
                                          widgets={
                                              'units': widgets.Select(
                                                  attrs={
                                                      'disabled': 'True'
                                                  }
                                              )
                                          }
                                          )
IngredientFormSetUpdate = inlineformset_factory(Recipe, Ingredient, fields=['product', 'quantity'],  # 'units'],
                                                extra=0,
                                                can_delete=True)
