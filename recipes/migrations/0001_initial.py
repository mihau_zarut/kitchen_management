# Generated by Django 3.2.12 on 2022-02-01 12:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=30, unique=True)),
                ('available_quantity', models.FloatField(default=0)),
                ('unit', models.CharField(choices=[('teaspoon', 'te'), ('tablespoon', 'ta'), ('fluid ounce', 'fl'), ('cup', 'cu'), ('pint', 'pi'), ('quart', 'qu'), ('gallon', 'ga'), ('ml', 'ml'), ('l', 'l'), ('po', 'pound'), ('ou', 'ounce'), ('mg', 'mg'), ('g', 'g'), ('kg', 'kg'), ('piece', 'piece')], default='piece', max_length=30)),
                ('price_per_unit', models.FloatField(default=0)),
            ],
            options={
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, default='Dish', max_length=30, unique=True)),
                ('description', models.TextField(blank=True)),
                ('prep_time_in_minutes', models.PositiveIntegerField(default=1, verbose_name='Preparation time in minutes')),
                ('cook_time_in_minutes', models.PositiveIntegerField(default=1, verbose_name='Cooking time in minutes')),
                ('available_to_prepare', models.BooleanField(default=True)),
                ('cost_of_ingredients', models.FloatField(default=0)),
            ],
            options={
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.FloatField(default=1)),
                ('units', models.CharField(blank=True, max_length=30)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='recipes.product')),
                ('recipe', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ingredients', to='recipes.recipe')),
            ],
        ),
    ]
