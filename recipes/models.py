from django.db import models, transaction
from django.db.models import Sum, F
from django.urls import reverse_lazy
from django.db import transaction
from django_extensions.db.models import TimeStampedModel

volume = [
    'teaspoon',
    'tablespoon',
    'fluid ounce',
    'cup',
    'pint',
    'quart',
    'gallon',
    'ml',
    'l'
]

volume_shorts = {vol: vol[:2] for vol in volume}

weight = [
    'pound',
    'ounce',
    'mg',
    'g',
    'kg',
]

weight_shorts = {wght[:2]: wght for wght in weight}

piece = ['piece']
piece_shorts = {pc: pc for pc in piece}

MEASUREMENT_UNITS = volume_shorts | weight_shorts | piece_shorts
MEASUREMENT_UNITS = MEASUREMENT_UNITS.items()


class Product(models.Model):
    name = models.CharField(db_index=True, max_length=30, unique=True)
    available_quantity = models.FloatField(default=0)
    unit = models.CharField(max_length=30, choices=MEASUREMENT_UNITS, default='piece')
    price_per_unit = models.FloatField(default=0)

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return f'{self.name} in unit: {self.unit}'

    def get_absolute_url(self):
        return reverse_lazy('product-detail-view', args=[self.pk])

    @transaction.atomic
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # POST_SAVE SIGNAL
        for ingredient in self.ingredient_set.all():
            ingredient.save()


class Recipe(models.Model):
    name = models.CharField(db_index=True, max_length=30, default=f'Dish', unique=True)
    description = models.TextField(blank=True)
    prep_time_in_minutes = models.PositiveIntegerField(default=1, verbose_name='Preparation time in minutes')
    cook_time_in_minutes = models.PositiveIntegerField(default=1, verbose_name='Cooking time in minutes')
    available_to_prepare = models.BooleanField(default=True)
    cost_of_ingredients = models.FloatField(default=0)

    class Meta:
        ordering = ['-pk', ]

    @property
    def ingredients_number(self):
        return self.ingredients.all().count()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse_lazy('recipe-detail-view', args=[self.pk])

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self.ingredients.count() == 0:
            self.cost_of_ingredients = 0
        else:
            self.cost_of_ingredients = self.ingredients.all().aggregate(
                total_cost=Sum(F('quantity') * F('product__price_per_unit')))['total_cost']
        super().save(*args, **kwargs)


class Ingredient(models.Model):
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    quantity = models.FloatField(default=1)
    units = models.CharField(max_length=30, blank=True)  # choices=MEASUREMENT_UNITS, blank=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='ingredients', null=True)

    def __str__(self):
        if self.units == '':
            return f'{self.quantity} of {self.product}'
        return f'{self.quantity} {self.units} of {self.product.name}'

    @transaction.atomic
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        self.units = self.product.unit
        if self.recipe is not None:
            if self.quantity > self.product.available_quantity:
                self.recipe.available_to_prepare = False
            else:
                self.recipe.available_to_prepare = True
            self.recipe.save()

    @transaction.atomic
    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        self.recipe.save()

# class PreparedRecipes(TimeStampedModel):
#     recipes = models.ForeignKey(Recipe, on_delete=models.DO_NOTHING)
