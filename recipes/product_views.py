from .models import Product
from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    DetailView,
    DeleteView
)
from django.urls import reverse_lazy


class ProductList(ListView):
    model = Product
    template_name = 'product/product_list.html'
    context_object_name = 'products'
    paginate_by = 25


class ProductDetail(DetailView):
    model = Product
    template_name = 'product/product_detail.html'
    context_object_name = 'product'


class ProductCreate(CreateView):
    model = Product
    template_name = 'product/product_create.html'
    success_url = reverse_lazy('product-list-view')
    fields = '__all__'


class ProductUpdate(UpdateView):
    model = Product
    template_name = 'product/product_update.html'
    fields = [
        'name',
        'available_quantity',
        'unit',
        'price_per_unit'
    ]


class ProductDelete(DeleteView):
    model = Product
    template_name = 'product/product_confirm_delete.html'
    success_url = reverse_lazy('product-list-view')
