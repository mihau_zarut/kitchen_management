from django.contrib import messages
from django.shortcuts import redirect
from django.views import View
from django.views.generic.detail import SingleObjectMixin

from .forms import IngredientFormSet, RecipeForm, IngredientFormSetUpdate
from .models import Recipe, Ingredient
from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    DetailView,
    DeleteView
)
from django.urls import reverse_lazy


class RecipeList(ListView):
    model = Recipe
    template_name = 'recipe/recipe_list.html'
    context_object_name = 'recipes'
    paginate_by = 25


class RecipeDetail(DetailView):
    model = Recipe
    template_name = 'recipe/recipe_detail.html'
    context_object_name = 'recipe'


class RecipeCreate(CreateView):
    model = Recipe
    template_name = 'recipe/recipe_create.html'
    success_url = reverse_lazy('recipe-list-view')

    fields = [
        'name',
        'prep_time_in_minutes',
        'cook_time_in_minutes'
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ingredient_formset = IngredientFormSet()
        recipe_form = RecipeForm()
        context['ingredient_formset'] = ingredient_formset
        context['recipe_form'] = recipe_form
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        ingredient_formset = IngredientFormSet(request.POST)
        recipe_form = RecipeForm(request.POST)
        if recipe_form.is_valid():
            new_recipe = recipe_form.instance
            new_recipe.save()

            for form in ingredient_formset:
                if form.is_valid():
                    new_ingredient = form.instance
                    new_ingredient.recipe = new_recipe
                    new_ingredient.save()
            return redirect('recipe-list-view')
        return super().post(request, *args, **kwargs)


class RecipeUpdate(UpdateView):
    model = Recipe
    template_name = 'recipe/recipe_update.html'
    fields = [
        'name',
        'description',
        'prep_time_in_minutes',
        'cook_time_in_minutes',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ingredient_formset = IngredientFormSetUpdate(instance=self.get_object())
        recipe_form = RecipeForm(instance=self.get_object())
        context['ingredient_formset'] = ingredient_formset
        context['recipe_form'] = recipe_form
        return context

    def post(self, request, *args, **kwargs):
        recipe = self.get_object()
        recipe_form = RecipeForm(request.POST, instance=recipe)
        ingredient_formset = IngredientFormSetUpdate(request.POST, instance=recipe)
        if recipe_form.is_valid():
            new_recipe = recipe_form.instance

            for ingredient_form in ingredient_formset:
                if ingredient_form.is_valid():
                    new_ingredient = ingredient_form.instance
                    if ingredient_form.cleaned_data['DELETE']:
                        Ingredient.objects.get(id=new_ingredient.id).delete()
                    else:
                        if new_ingredient.product is not None:
                            new_ingredient.recipe = new_recipe
                            new_ingredient.save()
            new_recipe.save()
            new_recipe.refresh_from_db()

            return redirect('recipe-detail-view', recipe.pk)
        return super().post(request, *args, **kwargs)


class RecipeDelete(DeleteView):
    model = Recipe
    template_name = 'recipe/recipe_confirm_delete.html'
    success_url = reverse_lazy('recipe-list-view')


def recipe_make(request, pk):
    recipe = Recipe.objects.get(id=pk)
    if recipe.available_to_prepare:
        for ingredient in recipe.ingredients.all():
            ingredient.product.available_quantity -= ingredient.quantity
            ingredient.product.save()
            updated_av_quantity = ingredient.product.available_quantity
            print(updated_av_quantity)
        messages.success(request, 'Recipe used ingredients!')

        return redirect('recipe-detail-view', recipe.pk)
    else:
        messages.error(request, 'Recipe is not available with existing products in store!')
        return redirect('recipe-list-view')
