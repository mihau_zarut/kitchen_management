from django.test import TestCase
from recipes.forms import IngredientFormSet, ProductForm, RecipeForm
from model_bakery import baker
from model_bakery.recipe import Recipe as BakeryRecipe


class TestForms(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.product = baker.make('recipes.Product')
        cls.recipe = BakeryRecipe(
            'recipes.Recipe'
        )

    def test_product_model_form_valid_data(self):
        form = ProductForm(data={
            'name': 'onion',
            'available_quantity': 1,
            'unit': 'kg',
            'price_per_unit': 2
        })
        self.assertTrue(form.is_valid())

    def test_product_model_form_invalid_data(self):
        form = ProductForm(data={})
        self.assertFalse(form.is_valid())

    def test_recipe_model_form_valid_data(self):
        form = RecipeForm(data={
            'name': 'spaghetti',
            'description': 'descriptive description',
            'prep_time_in_minutes': '15',
            'cook_time_in_minutes': '100',
        })
        self.assertTrue(form.is_valid())

    def test_recipe_model_form_invalid_data(self):
        form = RecipeForm(data={})
        self.assertFalse(form.is_valid())

    @staticmethod
    def instantiate_formset(formset_class, data, instance=None, initial=None):
        prefix = formset_class().prefix
        formset_data = {}
        for i, form_data in enumerate(data):
            for name, value in form_data.items():
                if isinstance(value, list):
                    for j, inner in enumerate(value):
                        formset_data['{}-{}-{}_{}'.format(prefix, i, name, j)] = inner
                else:
                    formset_data['{}-{}-{}'.format(prefix, i, name)] = value
        formset_data['{}-TOTAL_FORMS'.format(prefix)] = len(data)
        formset_data['{}-INITIAL_FORMS'.format(prefix)] = 0

        if instance:
            return formset_class(formset_data, instance=instance, initial=initial)
        else:
            return formset_class(formset_data, initial=initial)

    def test_ingredient_formset_valid_data(self):
        formset = self.instantiate_formset(
            IngredientFormSet, [
                {
                    'product': self.product,
                    'quantity': 1,
                    'units': 'kg',
                },
            ]
        )
        self.assertTrue(formset.is_valid())

    def test_ingredient_formset_invalid_data(self):
        form = IngredientFormSet(data={})
        self.assertFalse(form.is_valid())
