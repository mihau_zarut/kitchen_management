import lorem
from django.test import TestCase
from model_bakery import baker
from model_bakery.recipe import Recipe as BakeryRecipe
from recipes.models import Recipe, Product, Ingredient


class TestProduct(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.product = baker.make('recipes.Product', name='onion')

    def test_product_string_representation(self):
        self.assertEquals(self.product.__str__(), 'onion in unit: piece')

    def test_product_get_absolute_url(self):
        expected_url = f'/product/{self.product.pk}/'
        returned_url = self.product.get_absolute_url()
        self.assertEquals(expected_url, returned_url)


class TestIngredient(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.ingredient = baker

    def test_ingredient_same_units_as_its_product(self):
        product = Product(
            name='potato',
            available_quantity=10,
            unit='kg',
            price_per_unit=2
        )
        product.save()
        ingredient = Ingredient(
            product=product,
            quantity=5,
        )
        ingredient.save()
        self.assertEquals(product.unit, ingredient.units)


class TestRecipeAndIngredients(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.recipe = Recipe.objects.create(name='ravioli')
        product1 = Product.objects.create(
            name='onion',
            unit='kg'
        )
        cls.product2 = Product.objects.create(
            name='garlic',
            price_per_unit=10,
        )
        product3 = Product.objects.create(
            name='potato',
            available_quantity=200
        )

        cls.recipe1 = Recipe.objects.create(
            name='Recipe1',
            description=lorem.text(),
        )
        cls.recipe2 = Recipe.objects.create(
            name='Recipe2',
            description=lorem.text(),
        )

        cls.ingredient1 = Ingredient.objects.create(
            product=product1,
            quantity=10,
            recipe=cls.recipe1
        )

        cls.ingredient2 = Ingredient.objects.create(
            product=product3,
            quantity=10,
            recipe=cls.recipe2
        )
        cls.ingredient_recipe = BakeryRecipe(
            'recipes.Ingredient',
            product=product1,
        )
        cls.spaghetti_recipe = Recipe.objects.create(
            name='spaghetti420',
            description=lorem.text(),
        )

    def test_ingredient_string_representation(self):
        self.assertEquals(self.ingredient1.__str__(), '10 kg of onion')
        self.assertEquals(self.ingredient2.__str__(), '10 piece of potato')

    def test_recipe_string_representation(self):
        self.assertEquals(str(self.recipe), 'ravioli')

    def test_recipe_get_absolute_url(self):
        expected_url = f'/recipe/{self.recipe.pk}/'
        returned_url = self.recipe.get_absolute_url()
        self.assertEquals(expected_url, returned_url)

    def test_ingredients_number(self):
        self.recipe.ingredients.set(self.ingredient_recipe.make(_quantity=4))
        expected = 4
        returned = self.recipe.ingredients_number
        self.assertEquals(expected, returned)

    def test_recipe_create_proper_availability_parameter(self):
        returned1 = self.ingredient1.recipe.available_to_prepare
        returned2 = self.ingredient2.recipe.available_to_prepare

        self.assertEquals(returned1, False)
        self.assertEquals(returned2, True)

    def test_recipe_create_proper_units_parameter(self):
        self.assertEquals(self.ingredient1.units, self.ingredient1.product.unit)
        self.assertEquals(self.ingredient2.units, self.ingredient2.product.unit)

    def test_recipe_total_cost_add_ingredient(self):
        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 0)
        spaghetti_ingredient = Ingredient.objects.create(
            product=self.product2,
            recipe=self.spaghetti_recipe,
            quantity=10
        )

        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 100)

    def test_recipe_total_cost_remove_and_update_ingredient(self):
        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 0)
        spaghetti_ingredient = Ingredient.objects.create(
            product=self.product2,
            recipe=self.spaghetti_recipe,
            quantity=15
        )
        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 150)
        spaghetti_ingredient.quantity = 7
        spaghetti_ingredient.save()
        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 70)

        spaghetti_ingredient = self.spaghetti_recipe.ingredients.first()
        spaghetti_ingredient.delete()
        self.assertEquals(self.spaghetti_recipe.cost_of_ingredients, 0)
