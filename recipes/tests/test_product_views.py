from django.test import TestCase, Client
from django.urls import reverse
from recipes.models import Product


class TestViews(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.product = Product.objects.create(
            name='onion',
            available_quantity=4,
            unit='l',
            price_per_unit=2
        )
        cls.product_list_url = reverse('product-list-view')
        cls.product_create_url = reverse('product-create-view')
        cls.product_detail_url = reverse('product-detail-view', args=[cls.product.pk])
        cls.product_update_url = reverse('product-update-view', args=[cls.product.pk])
        cls.product_delete_url = reverse('product-delete-view', args=[cls.product.pk])

    def test_product_list(self):
        response = self.client.get(self.product_list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product_list.html')

    def test_product_detail(self):
        response = self.client.get(self.product_detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product_detail.html')

    def test_product_update_put(self):
        new_data = {
            "name": "ham",
            "available_quantity": 14,
            "unit": "kg",
            "price_per_unit": 12,
        }
        response = self.client.post(self.product_update_url, data=new_data)
        self.product.refresh_from_db()
        self.assertNotEquals(self.product.name, 'onion')
        self.assertEquals(self.product.name, 'ham')

        self.assertNotEquals(self.product.available_quantity, 4)
        self.assertEquals(self.product.available_quantity, 14)

        self.assertNotEquals(self.product.unit, 'l')
        self.assertEquals(self.product.unit, 'kg')

        self.assertNotEquals(self.product.price_per_unit, 2)
        self.assertEquals(self.product.price_per_unit, 12)

    def test_product_delete(self):
        count = Product.objects.count()
        response = self.client.delete(self.product_delete_url)
        self.assertEquals(Product.objects.count(), count - 1)

    def test_product_create(self):
        product_count = Product.objects.count()
        response = self.client.post(self.product_create_url, {
            'name': 'salami',
            'available_quantity': 2,
            'unit': 'kg',
            'price_per_unit': 123,
        })
        new_product_count = Product.objects.count()
        self.assertEquals(response.status_code, 302)
        self.assertEquals(product_count + 1, new_product_count)
