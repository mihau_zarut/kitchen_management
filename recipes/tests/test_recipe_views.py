import lorem
from django.test import TestCase, Client
from django.urls import reverse
from model_bakery import baker

from recipes.models import Recipe, Product, Ingredient


class TestViews(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()

        cls.recipe = baker.make('recipes.Recipe',
                                name='spaghetti',
                                description='delicious spaghetti confetti',
                                prep_time_in_minutes=10,
                                cook_time_in_minutes=25,
                                )
        cls.recipe_list_url = reverse('recipe-list-view')
        cls.recipe_create_url = reverse('recipe-create-view')
        cls.recipe_detail_url = reverse('recipe-detail-view', args=[cls.recipe.pk])
        cls.recipe_delete_url = reverse('recipe-delete-view', args=[cls.recipe.pk])

        prod1 = Product.objects.create(name='product1', available_quantity=0, unit='kg')
        prod2 = Product.objects.create(name='product2', available_quantity=9, unit='l')

        cls.recipe1 = Recipe.objects.create(
            name='Recipe1',
            description=lorem.text(),
        )

        cls.recipe2 = Recipe.objects.create(
            name='Recipe2',
            description=lorem.text(),
        )

        cls.ingredient1 = Ingredient.objects.create(
            product=prod1,
            quantity=4,
            recipe=cls.recipe1
        )
        cls.ingredient2 = Ingredient.objects.create(
            product=prod2,
            quantity=4,
            recipe=cls.recipe2

        )
        cls.ingredient3 = Ingredient.objects.create(
            product=prod2,
            quantity=15,
        )

    def test_recipe_list(self):
        response = self.client.get(self.recipe_list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'recipe/recipe_list.html')

    def test_recipe_detail(self):
        response = self.client.get(self.recipe_detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'recipe/recipe_detail.html')

    def test_recipe_update(self):
        recipe_to_update = self.recipe1
        recipe_update_url = reverse('recipe-update-view', args=[recipe_to_update.pk])

        new_data = {
            "name": "Absolutely new recipe",
            "description": "Some fancy description",
            "prep_time_in_minutes": 100,
            "cook_time_in_minutes": 200,
        }

        response = self.client.post(recipe_update_url, data=new_data)

        recipe_to_update.refresh_from_db()

        self.assertNotEquals(recipe_to_update.name, 'Recipe1')
        self.assertEquals(recipe_to_update.name, 'Absolutely new recipe')

        self.assertEquals(recipe_to_update.description, 'Some fancy description')

    def test_recipe_update_ingredients(self):
        recipe_to_update = self.recipe1
        ingredients = recipe_to_update.ingredients.all()
        ingredients.delete()
        self.assertEquals(recipe_to_update.ingredients.count(), 0)
        self.ingredient3.recipe = recipe_to_update
        self.ingredient2.recipe = recipe_to_update
        self.ingredient3.save()
        self.ingredient2.save()
        self.assertEquals(recipe_to_update.ingredients.count(), 2)

    def test_recipe_delete(self):
        count = Recipe.objects.count()
        response = self.client.delete(self.recipe_delete_url)
        self.assertEquals(Recipe.objects.count(), count - 1)

    def test_recipe_create(self):
        recipe_count = Recipe.objects.count()

        response = self.client.post(self.recipe_create_url, {
            'name': 'lasagna',
            'description': 'delicious lasagna confetti',
            'prep_time_in_minutes': 10,
            'cook_time_in_minutes': 20,
        })
        new_recipe_count = Recipe.objects.count()

        self.assertEquals(response.status_code, 302)
        self.assertEquals(recipe_count + 1, new_recipe_count)
