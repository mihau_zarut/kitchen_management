from django.test import TestCase
from django.urls import resolve, reverse
from recipes.recipe_views import RecipeList, RecipeCreate, RecipeDetail, RecipeUpdate, RecipeDelete
from recipes.product_views import ProductList, ProductCreate, ProductDetail, ProductUpdate, ProductDelete


class TestUrls(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.recipe_pk = 1
        cls.product_pk = 1

    def test_product_list_url_is_resolved(self):
        url = reverse('product-list-view')
        self.assertEquals(resolve(url).func.__name__, ProductList.as_view().__name__)

    def test_product_create_url_is_resolved(self):
        url = reverse('product-create-view')
        self.assertEquals(resolve(url).func.__name__, ProductCreate.as_view().__name__)

    def test_product_detail_url_is_resolved(self):
        url = reverse('product-detail-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, ProductDetail.as_view().__name__)

    def test_product_update_url_is_resolved(self):
        url = reverse('product-update-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, ProductUpdate.as_view().__name__)

    def test_product_delete_url_is_resolved(self):
        url = reverse('product-delete-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, ProductDelete.as_view().__name__)

    def test_recipe_list_url_is_resolved(self):
        url = reverse('recipe-list-view')
        self.assertEquals(resolve(url).func.__name__, RecipeList.as_view().__name__)

    def test_recipe_create_url_is_resolved(self):
        url = reverse('recipe-create-view')
        self.assertEquals(resolve(url).func.__name__, RecipeCreate.as_view().__name__)

    def test_recipe_detail_url_is_resolved(self):
        url = reverse('recipe-detail-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, RecipeDetail.as_view().__name__)

    def test_recipe_update_url_is_resolved(self):
        url = reverse('recipe-update-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, RecipeUpdate.as_view().__name__)

    def test_recipe_delete_url_is_resolved(self):
        url = reverse('recipe-delete-view', args=[self.product_pk])
        self.assertEquals(resolve(url).func.__name__, RecipeDelete.as_view().__name__)
