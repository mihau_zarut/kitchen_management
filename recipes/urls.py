from django.urls import path, include
from .product_views import (
    ProductList,
    ProductDetail,
    ProductCreate,
    ProductDelete,
    ProductUpdate
)
from .recipe_views import (
    RecipeList,
    RecipeDetail,
    RecipeCreate,
    RecipeDelete,
    RecipeUpdate,
    recipe_make
    # RecipeMake,
)

urlpatterns = [
    path('', RecipeList.as_view(), name='product-list-view'),

    path('product/', ProductList.as_view(), name='product-list-view'),
    path('product/new/', ProductCreate.as_view(), name='product-create-view'),
    path('product/<int:pk>/', ProductDetail.as_view(), name='product-detail-view'),
    path('product/<int:pk>/update/', ProductUpdate.as_view(), name='product-update-view'),
    path('product/<int:pk>/delete/', ProductDelete.as_view(), name='product-delete-view'),

    path('recipe/', RecipeList.as_view(), name='recipe-list-view'),
    path('recipe/new/', RecipeCreate.as_view(), name='recipe-create-view'),
    path('recipe/<int:pk>/', RecipeDetail.as_view(), name='recipe-detail-view'),
    path('recipe/<int:pk>/update/', RecipeUpdate.as_view(), name='recipe-update-view'),
    path('recipe/<int:pk>/delete/', RecipeDelete.as_view(), name='recipe-delete-view'),

    # PREPARE RECIPE AND USE THE INGREDIENTS
    path('recipe/<int:pk>/make', recipe_make, name='recipe-make-view'),

]
