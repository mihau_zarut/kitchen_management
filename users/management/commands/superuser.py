from django.core.management.base import BaseCommand
from rest_framework.authtoken.models import Token
from django.conf import settings

from users.models import KitchenUser


class Command(BaseCommand):
    help = 'Create Kitchen Superuser for development'

    def handle(self, *args, **kwargs):
        if settings.DEBUG:
            admin_user = KitchenUser.objects.filter(email='admin@example.com')
            if admin_user.exists():
                self.stdout.write(
                    self.style.WARNING('This user already exists!\nDeleting the user and creating new again!')
                )
                admin_user.delete()
            admin_user = KitchenUser(email='admin@example.com')
            admin_user.set_password('1234')
            admin_user.is_staff = True
            admin_user.is_superuser = True
            admin_user.save()
            if not Token.objects.filter(user=admin_user).exists():
                Token.objects.create(user=admin_user)
            self.stdout.write(self.style.SUCCESS(f'User {admin_user.email} created with success!'))
